package com.semleti.sbrjm2.back;

import com.semleti.sbrjm2.data.Address;
import com.semleti.sbrjm2.data.Hotel;
import com.semleti.sbrjm2.data.Review;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DbSeeder implements CommandLineRunner {

    private HotelRepository hotelRepository;

    public DbSeeder(HotelRepository hotelRepository) {
        this.hotelRepository = hotelRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        Hotel marriot = new Hotel("Marriot",
                120,
                new Address("London","Germany"),
                Arrays.asList(new Review("John",8,true))
        );

        Hotel ibis = new Hotel("Ibis",
                50,
                new Address("Paris","Australia"),
                Arrays.asList(new Review("Marie",4,false))
        );

        this.hotelRepository.deleteAll();

        List<Hotel> hotels = Arrays.asList(marriot, ibis);
        this.hotelRepository.saveAll(hotels);
    }
}
