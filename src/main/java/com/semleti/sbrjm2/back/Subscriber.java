package com.semleti.sbrjm2.back;

import com.semleti.sbrjm2.data.Hotel;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Subscriber {
    @Autowired
    private HotelRepository hotelRepository;

    @RabbitListener(queues="${jsa.rabbitmq.queue}")
    public void receivedMessage(String msg) {
        System.out.println("Received Message: " + msg);
    }

    @RabbitListener(queues="${jsa.rabbitmq.queueDeleteId}")
    public void receivedMessageDeleteId(String id) {
        System.out.println("Delete Id: " + id);
        this.hotelRepository.deleteById(id);
    }

    @RabbitListener(queues="${jsa.rabbitmq.queueDeleteObject}")
    public void receivedMessageDeleteObject(Hotel hotel) {
        System.out.println("Delete Hotel: " + hotel.getName());
        this.hotelRepository.delete(hotel);
    }

    @RabbitListener(queues="${jsa.rabbitmq.queueSave}")
    public void receivedMessageSave(Hotel hotel) {
        System.out.println("Save Hotel: " + hotel.getName());
        this.hotelRepository.save(hotel);
    }

}
