package com.semleti.sbrjm2.back;

import com.semleti.sbrjm2.data.Hotel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HotelRepository extends MongoRepository<Hotel, String> {
    List<Hotel> findByPricePerNightLessThan(int max);

    @Query("{ 'address.city' : ?0 }")
    List<Hotel> findByCity(String city);

    @Query("{ 'address.country' : ?0 }")
    List<Hotel> findByCountry(String country);

    @Query("{ 'pricePerNight' : { $lt: ?0 }, 'reviews.rating' : { $gt : ?1 } }")
    List<Hotel> findByRecommended(int maxPrice, int minRating);

}
