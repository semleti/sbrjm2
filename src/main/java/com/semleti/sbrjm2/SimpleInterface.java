package com.semleti.sbrjm2;

public interface SimpleInterface {

    /**
     * Returns UUID generated on server side
     * @param clientId client ID
     */
    String doJob(int clientId);
}