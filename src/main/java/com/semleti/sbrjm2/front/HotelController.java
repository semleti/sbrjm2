package com.semleti.sbrjm2.front;

import com.semleti.sbrjm2.back.HotelRepository;
import com.semleti.sbrjm2.data.Hotel;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Optional;

@Component
@Path("/api/hotels")
public class HotelController {
    @Autowired
    private HotelRepository hotelRepository;

    @Autowired
    @Qualifier("Rabbit2")
    private AmqpTemplate amqpTemplate;



    @Value("${jsa.rabbitmq.exchange}")
    private String exchange;

    @Value("${jsa.rabbitmq.routingKey}")
    private String routingKey;



    @Value("${jsa.rabbitmq.exchangeSave}")
    private String exchangeSave;

    @Value("${jsa.rabbitmq.routingKeySave}")
    private String routingKeySave;



    @Value("${jsa.rabbitmq.exchangeDelete}")
    private String exchangeDelete;

    @Value("${jsa.rabbitmq.routingKeyDeleteObject}")
    private String routingKeyDeleteObject;

    @Value("${jsa.rabbitmq.routingKeyDeleteId}")
    private String routingKeyDeleteId;



    @PUT
    @Consumes("application/json")
    public void insert(Hotel hotel) {
        System.out.println(hotel);
        amqpTemplate.convertAndSend(exchangeSave, routingKeySave, hotel);
    }

    @POST
    @Consumes("application/json")
    public void update(Hotel hotel) {
        System.out.println(hotel);
        amqpTemplate.convertAndSend(exchangeSave, routingKeySave, hotel);
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(Hotel hotel) {
        System.out.println("Delete object: " + hotel.getName());
        amqpTemplate.convertAndSend(exchangeDelete, routingKeyDeleteObject, hotel);
    }

    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") String id) {
        System.out.println("delete id: " + id);
        amqpTemplate.convertAndSend(exchangeDelete, routingKeyDeleteId, id);
    }




    @GET
    @Produces("application/json")
    public List<Hotel> getAll() {
        return this.hotelRepository.findAll();
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Optional<Hotel> getById(@PathParam("id") String id) {
        return this.hotelRepository.findById(id);
    }

    @GET
    @Path("/price/{maxPrice}")
    @Produces("application/json")
    public List<Hotel> getByPricePerNight(@PathParam("maxPrice") int maxPrice) {
        return this.hotelRepository.findByPricePerNightLessThan(maxPrice);
    }

    @GET
    @Path("/city/{city}")
    @Produces("application/json")
    public List<Hotel> getByCity(@PathParam("city") String city) {
        return this.hotelRepository.findByCity(city);
    }

    @GET
    @Path("/country/{country}")
    @Produces("application/json")
    public List<Hotel> getByCountry(@PathParam("country") String country) {
        return this.hotelRepository.findByCountry(country);
    }

    @GET
    @Path("/recommended")
    @Produces("application/json")
    public List<Hotel> getRecommended() {
        return this.hotelRepository.findByRecommended(70, 3);
    }

}
