package com.semleti.sbrjm2.config;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {
    @Value("${jsa.rabbitmq.queue}")
    private String queueName;

    @Value("${jsa.rabbitmq.exchange}")
    private String exchange;

    @Value("${jsa.rabbitmq.routingKey}")
    private String routingKey;



    @Value("${jsa.rabbitmq.queueSave}")
    private String queueSave;

    @Value("${jsa.rabbitmq.exchangeSave}")
    private String exchangeSave;

    @Value("${jsa.rabbitmq.routingKeySave}")
    private String routingKeySave;



    @Value("${jsa.rabbitmq.queueDeleteId}")
    private String queueNameDeleteId;

    @Value("${jsa.rabbitmq.queueDeleteObject}")
    private String queueNameDeleteObject;

    @Value("${jsa.rabbitmq.exchangeDelete}")
    private String exchangeDelete;

    @Value("${jsa.rabbitmq.routingKeyDeleteId}")
    private String routingKeyDeleteId;

    @Value("${jsa.rabbitmq.routingKeyDeleteObject}")
    private String routingKeyDeleteObject;



    @Bean
    Queue queue() {
        return new Queue(queueName, false);
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange(exchange);
    }

    @Bean
    Binding binding(Queue queue, DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(routingKey);
    }


    @Bean
    Queue queueSave() {
        return new Queue(queueSave, false);
    }

    @Bean
    DirectExchange exchangeSave() {
        return new DirectExchange(exchangeSave);
    }

    @Bean
    Binding bindingSave(Queue queueSave, DirectExchange exchangeSave) {
        return BindingBuilder.bind(queueSave).to(exchangeSave).with(routingKeySave);
    }



    @Bean
    public Queue queueDeleteId() {
        return new Queue(queueNameDeleteId, false);
    }

    @Bean
    public Queue queueDeleteObject() {
        return new Queue(queueNameDeleteObject, false);
    }

    @Bean
    DirectExchange exchangeDelete() {
        return new DirectExchange(exchangeDelete);
    }

    @Bean
    public Binding bindingDeleteId(Queue queueDeleteId, DirectExchange exchangeDelete) {
        return BindingBuilder.bind(queueDeleteId)
                .to(exchangeDelete)
                .with(routingKeyDeleteId);
    }

    @Bean
    public Binding bindingDeleteObject(Queue queueDeleteObject, DirectExchange exchangeDelete) {
        return BindingBuilder.bind(queueDeleteObject)
                .to(exchangeDelete)
                .with(routingKeyDeleteObject);
    }



    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }


    @Bean("Rabbit2")
    public AmqpTemplate rabbitTemplate2(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }

}