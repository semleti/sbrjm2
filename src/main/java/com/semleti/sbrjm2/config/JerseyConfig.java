package com.semleti.sbrjm2.config;

import com.semleti.sbrjm2.front.HotelController;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;


@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {

        register(CORSResponseFilter.class);
        register(HotelController.class);
    }
}