package com.semleti.sbrjm2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sbrjm2Application {

    public static void main(String[] args) {
        SpringApplication.run(Sbrjm2Application.class, args);
    }

}

